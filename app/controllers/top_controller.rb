class TopController < ApplicationController
    def main
        if session[:login_uid]
           render 'main'
           
        else
           render 'login'
        end
    end
    def login
        logger.debug('******************'+params[:uid]+params[:pass])
        user = User.authenticate(params[:uid], params[:pass])
        if user
 
               session[:login_uid] = params[:uid]
               redirect_to top_main_path

        else
            render 'error'
        end
    end
    
    def logout
        session.delete(:login_uid)
        @user = nil
        flash[:notice] = "ログアウトしました。"
        redirect_to top_main_path
        
    end
    
end
