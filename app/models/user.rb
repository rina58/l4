class User < ApplicationRecord
    validates :uid, uniqueness: true
    def self.authenticate(uid, pass)
        user = User.find_by(uid: uid)
        if   user and BCrypt::Password.new(user.pass) == pass
             User
        else
             nil
           
        end 
     
    end
end
